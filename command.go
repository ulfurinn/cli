package cli

import (
	"fmt"
	"io"
	"strings"
)

// Command defines an executable action. Commands can be nested.
type Command struct {
	Commands   []Command            // a list of subcommands
	Options    []Option             // a list of named options
	Args       []Option             // a list of positional arguments
	Name       string               // command name, used when parsing the argument list
	ShortName  string               // an alternative abbreviated name
	Usage      string               // usage documentation
	Before     func(*Context) error // a callback to run before the action
	Action     func(*Context) error // entry callback
	Completion func(*Context)       // an override for default shell completion
}

func (c *Command) hasName(name string) bool {
	return c.Name == name || c.ShortName == name
}

func (c *Command) findCommand(ctx *Context) {
	ctx.commands = append(ctx.commands, *c)
	if len(ctx.args) == 0 {
		return
	}
	a := ctx.args[0]
	for i := range c.Commands {
		if c.Commands[i].hasName(a) {
			ctx.args = ctx.args[1:]
			c.Commands[i].findCommand(ctx)
		}
	}
}

func (c *Command) expanded() map[string]Command {
	result := map[string]Command{}
	for _, subc := range c.Commands {
		for k, v := range subc.expanded() {
			result[c.Name+" "+k] = v
		}
	}
	result[c.Name] = *c
	return result
}

func (c *Command) appendHelp() {
	hasHelp := false
	hasHelpCommands := false
	for _, com := range c.Commands {
		if com.hasName("help") {
			hasHelp = true
		}
		if com.hasName("help-commands") {
			hasHelpCommands = true
		}
	}
	if !hasHelp {
		c.Commands = append(c.Commands, helpCommand)
	}
	if !hasHelpCommands {
		c.Commands = append(c.Commands, helpTreeCommand)
	}
}

func (c *Command) showCompletion(ctx *Context) {
	if c.Completion != nil {
		c.Completion(ctx)
		return
	}
	if missing := ctx.options.MissingValue; missing != nil {
		opt, positional := ctx.findOption(missing.Name)
		if f := opt.completion(); f != nil {
			showCompletion(ctx.app.Out, f(ctx, opt))
		}
		if !positional {
			return
		}
	}
	if ctx.parseError != nil {
		return
	}
	list := []string{}
	for _, cmd := range c.Commands {
		if cmd.Name != "help" && cmd.Name != "help-commands" {
			list = append(list, cmd.Name, cmd.ShortName)
		}
	}
	for _, opt := range c.Options {
		list = append(list, opt.CompletionStrings()...)
	}
	showCompletion(ctx.app.Out, list)
}

func showCompletion(out io.Writer, strings []string) {
	for _, str := range strings {
		if str != "" {
			fmt.Fprintln(out, str)
		}
	}
}

func StdCompletionFlags() string {
	return "$stdcomp=-fd"
}

func StdCompletion(*Context, Option) []string {
	return []string{StdCompletionFlags()}
}

// ValueListCompletion is a shell completion generator for option values suggesting possible alternatives from a given list.
func ValueListCompletion(ctx *Context, opt Option) []string {
	switch o := opt.(type) {
	case StringOption:
		return o.ValueList
	default:
		return []string{}
	}
}

// ValueListValidator is an option value validator checking against a given list of allowed values.
func ValueListValidation(ctx *Context, opt Option) error {
	// default values always pass
	if lowOpt := ctx.options.Lookup(opt.name()); lowOpt != nil && !lowOpt.Value.Explicit() {
		return nil
	}
	switch o := opt.(type) {
	case StringOption:
		givenValue := ctx.String(o.Name)
		for _, allowedValue := range o.ValueList {
			if givenValue == allowedValue {
				return nil
			}
		}
		return fmt.Errorf("%s accepts one of the following values: %s", o.Name, strings.Join(o.ValueList, ","))
	default:
		return nil
	}
}
