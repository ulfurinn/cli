package cli

import (
	"fmt"

	"bitbucket.org/ulfurinn/cli/flags"
)

// Context represents the parsed process arguments.
type Context struct {
	app        *App
	args       []string
	commands   []Command
	options    *flags.Set
	parseError error
}

// Arg returns an unparsed argument.
func (c *Context) Arg(i int) string {
	if i >= len(c.args) {
		return ""
	}
	return c.args[i]
}

// Args returns all unparsed arguments.
func (c *Context) Args() []string {
	cp := make([]string, len(c.args))
	copy(cp, c.args)
	return cp
}

// ArgLen returns the count of unparsed arguments.
func (c *Context) ArgLen() int {
	return len(c.args)
}

// String returns the value of an argument.
func (c *Context) String(name string) (v string) {
	opt := c.options.Lookup(name)
	if opt == nil {
		return
	}
	if strOpt, ok := opt.Value.(*flags.StringValue); ok && strOpt != nil {
		v = string(*strOpt)
	}
	return
}

// Bool returns the value of an argument.
func (c *Context) Bool(name string) (v bool) {
	opt := c.options.Lookup(name)
	if opt == nil {
		return
	}
	if boolOpt, ok := opt.Value.(*flags.BoolValue); ok && boolOpt != nil {
		v = bool(*boolOpt)
	}
	return
}

// Int returns the value of an argument.
func (c *Context) Int(name string) (v int) {
	opt := c.options.Lookup(name)
	if opt == nil {
		return
	}
	if intOpt, ok := opt.Value.(*flags.IntValue); ok && intOpt != nil {
		v = int(*intOpt)
	}
	return
}

// Float64 returns the value of an argument.
func (c *Context) Float64(name string) (v float64) {
	opt := c.options.Lookup(name)
	if opt == nil {
		return
	}
	if floatOpt, ok := opt.Value.(*flags.Float64Value); ok && floatOpt != nil {
		v = float64(*floatOpt)
	}
	return
}

// StringSlice returns the value of an argument.
func (c *Context) StringSlice(name string) (v []string) {
	opt := c.options.Lookup(name)
	if opt == nil {
		return
	}
	if sliceOpt, ok := opt.Value.(*StringSlice); ok && sliceOpt != nil {
		v = *sliceOpt
	}
	return
}

func (c *Context) command() *Command { return &c.commands[len(c.commands)-1] }

func (c *Context) run() (err error) {
	c.setupOptions()
	err = c.parseOptions()
	// we can't check the result now because with shell completion errors can be a legitimate case

	completion := c.Bool("generate-shell-completion")
	help := c.Bool("help")

	if completion {
		if err == nil || c.options.MissingValue != nil {
			c.command().showCompletion(c)
			err = nil
		}
		return
	}

	if c.options.MissingValue != nil {
		if !c.options.MissingValue.Optional {
			return fmt.Errorf("no value provided for argument %s", c.options.MissingValue.Name)
		}
	}

	if help {
		err = helpOptionAction(c)
		return
	}

	//	now we can check the result from parseOptions
	if err != nil {
		return
	}

	err = c.validateOptions()
	if err != nil {
		return err
	}

	for _, cmd := range c.commands {
		if cmd.Before != nil {
			if err := cmd.Before(c); err != nil {
				return err
			}
		}
	}

	if err == nil {
		if c.command().Action != nil {
			err = c.command().Action(c)
		} else if len(c.command().Commands) > 0 {
			err = helpOptionAction(c)
		}
	}

	return

}

func (c *Context) setupOptions() {
	if c.options == nil {
		c.options = flags.NewSet()
	}
	for i, com := range c.commands {
		for _, arg := range com.Args {
			//	only the direct command may take a positional
			if i == len(c.commands)-1 {
				arg.ApplyPositional(c.options)
			}
		}
		for _, opt := range com.Options {
			//	local options are not inherited by subcommands
			if i == len(c.commands)-1 || !opt.local() {
				opt.ApplyNamed(c.options)
			}
		}
	}
	helpOption.ApplyNamed(c.options)
	shellCompletionOption.ApplyNamed(c.options)
}

func (c *Context) parseOptions() (err error) {
	err = c.options.Parse(c.args)
	c.parseError = err
	c.args = c.options.Args()
	return
}

func (c *Context) validateOptions() error {
	for _, opt := range c.command().Options {
		if opt.validation() != nil {
			if err := opt.validation()(c, opt); err != nil {
				return nil
			}
		}
	}
	return nil
}

func (c *Context) findOption(name string) (option Option, positional bool) {
	for _, cmd := range c.commands {
		for _, opt := range cmd.Args {
			if opt.name() == name {
				option = opt
				positional = true
			}
		}
		for _, opt := range cmd.Options {
			if opt.name() == name {
				option = opt
			}
		}
	}
	return
}
