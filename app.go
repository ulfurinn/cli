package cli

import (
	"fmt"
	"io"
	"os"
)

// App defines the entire application interface.
type App struct {
	HideHelp bool
	Name     string
	Usage    string
	Main     Command // the top-level command
	Out      io.Writer
}

//	NewApp sets up a new app instance.
func NewApp() *App {
	return &App{
		Name: os.Args[0],
		Out:  os.Stdout,
	}
}

// Run parses the given argument list and executes the matching command.
func (a *App) Run(arguments []string) error {
	a.Main.appendHelp()
	ctx := &Context{
		app:  a,
		args: arguments,
	}
	a.Main.findCommand(ctx)
	return ctx.run()
}

// RunMain is an entry point suitable for most common cases.
func (a *App) RunMain() {
	if err := a.Run(os.Args[1:]); err != nil {
		fmt.Fprintln(os.Stderr, err)
		if exit, ok := err.(Exit); ok {
			os.Exit(exit.StatusCode)
		} else {
			os.Exit(1)
		}
	}
	os.Exit(0)
}
