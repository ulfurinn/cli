package cli

// Exit is an Error type returning which will cause the process to exit with the specified code.
type Exit struct {
	Err        error
	StatusCode int
}

func (e Exit) IsError() bool {
	return e.Err != nil
}

func (e Exit) Error() string {
	if e.IsError() {
		return e.Err.Error()
	}
	return ""
}
